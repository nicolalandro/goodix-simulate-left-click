#!/usr/bin/python3
import asyncio
from evdev import InputDevice, UInput, KeyEvent, ecodes
import evdev
import os
import sys
import subprocess
import time

async def main():
  for path in evdev.list_devices():
    dev = InputDevice(path)
    if dev.name == sys.argv[1]:
      await simulate_right_click(dev)
      return
  # Device not found
  sys.stderr.write("Failed to detect the specified device.\n")
  exit(1)

async def simulate_right_click(dev):
  scale_factor = int(os.environ['RCE_SCALE'])
  threshold = 80 * scale_factor
  min_interval = 0.5
  pos_x = -1
  pos_y = -1
  pos_last_x = -1
  pos_last_y = -1
  idev_caps = {
      ecodes.EV_REL: (ecodes.REL_X, ecodes.REL_Y),
      ecodes.EV_KEY: (ecodes.BTN_RIGHT, ecodes.BTN_TOUCH)
  }
  idev = UInput(idev_caps)
  trigger_task = None
  # The task function used to trigger right clicks
  async def trigger_right_click():
    await asyncio.sleep(min_interval)
    dX = abs(pos_last_x - pos_x)
    dY = abs(pos_last_y - pos_y)
    if dX <= threshold and dY <= threshold:
      idev.write(ecodes.EV_REL, ecodes.REL_X, 1)
      idev.write(ecodes.EV_REL, ecodes.REL_Y, 1)
      idev.write(ecodes.EV_KEY, ecodes.BTN_TOUCH, 0)
      idev.syn()
      idev.write(ecodes.EV_KEY, ecodes.BTN_RIGHT, 1)
      idev.syn()
      idev.write(ecodes.EV_KEY, ecodes.BTN_RIGHT, 0)
      idev.syn()
    trigger_task = None

  async for ev in dev.async_read_loop():
    if ev.type == ecodes.EV_ABS:
      abs_type = ecodes.ABS[ev.code]
      # Track the position of touch
      # Note that this position is not 1:1 to the screen resolution
      if abs_type == "ABS_X" or abs_type == "ABS_MT_POSITION_X":
        pos_x = ev.value
      elif abs_type == "ABS_Y" or abs_type == "ABS_MT_POSITION_Y":
        pos_y = ev.value
    elif ev.type == ecodes.EV_KEY:
      tev = KeyEvent(ev)
      if tev.keycode == 'BTN_TOUCH':
        if tev.keystate == KeyEvent.key_down:
          if trigger_task != None:
            trigger_task.cancel()
          trigger_task = asyncio.get_event_loop().create_task(trigger_right_click())
          pos_last_x = pos_x
          pos_last_y = pos_y
        elif tev.keystate == KeyEvent.key_up:
          if trigger_task != None:
            trigger_task.cancel()
          
  idev.close()

asyncio.get_event_loop().run_until_complete(main())
# vim: tabstop=2 softtabstop=2 shiftwidth=2 expandtab
