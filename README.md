# Goodix touch screen Simulate Right Click

This script allows a convertible tablet with goodix screen to use long press to simulate right click.

The install script will setup the systemd and set the path


## Install

```
./install.sh
# Service commands
# systemctl --user start simulate-left-click
# systemctl --user status simulate-left-click
# systemctl --user stop simulate-left-click
```
